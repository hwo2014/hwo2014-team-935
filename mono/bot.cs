using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;

public class Bot 
{

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		string track = null;
		if (args.Length > 4) track = args [4];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

            new Bot(reader, writer, new BotID(botName, botKey), track);
		}
	}

	private StreamWriter writer;
	private String selfColor;
	private String selfName;
	public	static bool pleaseActivateTheTurbo = false;
	public static int pleaseSwitch = 0;

    Bot(StreamReader reader, StreamWriter writer, BotID botID, string track = null) {
		this.writer = writer;
		string line;

		if (track != null) send(new JoinRace(botID, track, "", 1));
		else send (new Join(botID.name, botID.key));

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

			try
			{
				switch(msg.msgType) {
				case "carPositions":
					var carPositions = JsonConvert.DeserializeObject<List<CarPosition>> (msg.data.ToString ());
					WorldModel.currentGameTick = msg.gameTick;
					try
					{
						double t = run (carPositions);
						if (pleaseSwitch != 0) {
							send(new SwitchLane( pleaseSwitch > 0 ? Direction.Right : Direction.Left ));
							pleaseSwitch = 0;
						} if (pleaseActivateTheTurbo) {
							pleaseActivateTheTurbo = false;
							WorldModel.activeTurboDurationTicks = WorldModel.turboDurationTicks;
							WorldModel.activeTurboFactor = WorldModel.turboFactor;
							WorldModel.turboActivationTick = WorldModel.currentGameTick;
							WorldModel.turboAvailable = false;
							send (new EnableTurbo ());
						} else {
							send (new Throttle (t, WorldModel.currentGameTick));
						}
					} 
					// Gotta catch em all!
					catch (Exception e)
					{
						Logger.FlushAggregator ();
						Logger.Log (e.ToString ());
						send (new Throttle (0.5, WorldModel.currentGameTick));
					}
					break;
				case "join":
					Console.WriteLine("Joined");
					break;
				case "yourCar":
					var car = JsonConvert.DeserializeObject<Car> (msg.data.ToString ());
					selfColor = car.Color;
					selfName = car.Name;
					Console.WriteLine("My color is {0}", selfColor);
					break;
				case "gameInit":
					Console.WriteLine ("Race id " + msg.gameId + " init");
					var GameInit = JsonConvert.DeserializeObject<GameInit> (msg.data.ToString ());
					WorldModel.Race = GameInit.Race;
					SpeedModel.initialize();
					WorldModel.Enemies.Clear();
					foreach (var carDescription in GameInit.Race.Cars)
					{
						if (carDescription.Id.Color != selfColor) WorldModel.Enemies.Add( new CarModel() { Car = carDescription.Id, Dimensions = carDescription.Dimensions });
					}
					Logger.LogRace();
					break;
				case "gameEnd":
	                var gameEnd = JsonConvert.DeserializeObject<GameEnd>(msg.data.ToString());
	                Logger.LogMatrixEnd();
					Console.WriteLine("Race ended");
					break;
				case "gameStart":
	                // data = null
					send(new Ping());
					break;
	            case "tournamentEnd":
	                // data = null
					break;
	            case "crash":
	                var crashedCar = JsonConvert.DeserializeObject<Car>(msg.data.ToString());
	                break;
	            case "spawn":
	                var spawnedCar = JsonConvert.DeserializeObject<Car>(msg.data.ToString());
	                break;
	            case "lapFinished":
	                var lapFinished = JsonConvert.DeserializeObject<LapFinished>(msg.data.ToString());
	                break;
	            case "dnf":
	                var dnf = JsonConvert.DeserializeObject<DNF>(msg.data.ToString());
	                break;
	            case "finish":
	                var finishedCar = JsonConvert.DeserializeObject<Car>(msg.data.ToString());
	                break;
				case "turboAvailable":
					var turbo = JsonConvert.DeserializeObject<TurboAvailable> (msg.data.ToString ());
					if (!WorldModel.turboAvailable) {
						WorldModel.turboAvailable = true;
						WorldModel.turboDurationTicks = turbo.TurboDurationTicks;
						WorldModel.turboFactor = turbo.TurboFactor;
					}
					break;
	            default:
					break;
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine ("FUUUUUDEEEEEEUUUU");
			}
		}
	}

	private void updateModels ()
	{
		WorldModel.updateModel ();
		ThrottleModel.updateModel ();
		AngleModel.updateModel ();
		SpeedModel.update();
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

	private double run (IEnumerable<CarPosition> carPositions)
	{
		double t = 1;

		// Process cars and find self
		foreach (var carPosition in carPositions) {
			if (carPosition.Car.Color == selfColor) {
				WorldModel.Self = carPosition;
			} else {
				WorldModel.Enemies.Find (c => c.Color == carPosition.Color).updateCarModel (carPosition.PiecePosition);
			}
		}

		updateModels ();

		// Log self info
		Logger.Aggregate ("Lap", WorldModel.Self.PiecePosition.Lap);
		Logger.Aggregate ("Piece Index", WorldModel.Self.PiecePosition.PieceIndex);
		Logger.Aggregate ("In Piece Distance", WorldModel.Self.PiecePosition.InPieceDistance);
		Logger.Aggregate ("Total Distance", WorldModel.distances [WorldModel.currentGameTick]);
		Logger.Aggregate ("Speed", WorldModel.speeds [WorldModel.currentGameTick]);
		Logger.Aggregate ("Current Radius", WorldModel.Race.Track.Pieces [WorldModel.Self.PiecePosition.PieceIndex].Radius);
		Logger.Aggregate ("Angle", WorldModel.Self.Angle);
		Logger.Aggregate ("Start Lane Index", WorldModel.Self.PiecePosition.Lane.StartLaneIndex);
		Logger.Aggregate ("End Lane Index", WorldModel.Self.PiecePosition.Lane.EndLaneIndex);

		/*** Control logic starts here ***/

		int idx = WorldModel.Self.PiecePosition.PieceIndex;
		var pieces = WorldModel.Race.Track.Pieces;
		int piecesCount = pieces.Count;

		// Check for lane switching if we didnt send a switch message yet
		if (WorldModel.speeds[WorldModel.currentGameTick] > 0.2
			&& !WorldModel.willSwitchLanes 
		    && ThrottleModel.KnownCoefficients) {
			int nextSwitch = -1;
			int secondSwitch = -1;

			for (int i = idx + 1; i < idx + 2 + pieces.Count/2; ++i) {
				if (pieces [i % piecesCount].Switch) {
					if (nextSwitch == -1) {
						nextSwitch = i % piecesCount;
					} else {
						secondSwitch = i % piecesCount;
						break;
					}
				}
			}

			// only switch lanes right before a switch
			if (secondSwitch != -1 && nextSwitch == (idx + 1) % piecesCount) 
			{
				List<int> lanes = new List<int> ();
				var ticksToSecondSwitch = new Dictionary<int, int> ();

				lanes.Add(WorldModel.Self.LaneIndex);
				if (WorldModel.Self.LaneIndex + 1 < WorldModel.Race.Track.Lanes.Count) {
					lanes.Add(WorldModel.Self.LaneIndex + 1);
				}
				if (WorldModel.Self.LaneIndex - 1 >= 0) {
					lanes.Add(WorldModel.Self.LaneIndex - 1);
				}

				foreach (var lane in lanes) {
					double speed = WorldModel.speeds[WorldModel.currentGameTick];
					double distance = 0;
					int ticksToReachSecondSwitch = 0;
					int currentPieceIdx = idx;
					double inPieceDistance = WorldModel.Self.PiecePosition.InPieceDistance;

					int safeLoop = 0;
					while (currentPieceIdx != secondSwitch) {
						if (safeLoop ++> 300) break;

						distance += speed;

						double realPieceLength = WorldModel.Race.Track.Pieces [currentPieceIdx].RealLength (lane);

						if (speed + inPieceDistance >= realPieceLength) {
							inPieceDistance = inPieceDistance + speed - realPieceLength;
							currentPieceIdx = (currentPieceIdx + 1) % piecesCount;
						} else {
							inPieceDistance += speed;
						}

						++ticksToReachSecondSwitch;
					}

					ticksToSecondSwitch[lane] = ticksToReachSecondSwitch;
				}

				lanes = lanes.OrderBy (l => ticksToSecondSwitch[l]).ToList();
				int desiredLane = lanes.First();

				foreach (var lane in lanes) {					
					if (ticksToSecondSwitch[lane] < 5) continue; // fudeu

					bool collision = false;
					foreach (var enemy in WorldModel.Enemies) {
						if (enemy.LaneIndex != lane) continue;
						if (WorldModel.Race.Track.IsAAheadOfB(WorldModel.Self.PiecePosition, enemy.PiecePosition, enemy.LaneIndex)) continue;

						// We decrease 5 (MAGIC NUMBER!) ticks to account for the car length 8D
						var enemyFuturePosition = ThrottleModel.GetFuturePosition(enemy.speed, ticksToSecondSwitch[lane] - 5, enemy.Color);
						if (!WorldModel.Race.Track.IsPieceABetweenBandC (enemyFuturePosition.Item1, nextSwitch, secondSwitch)) continue;

						// So the enemy was ahead of us, and when we would have reached the second switch,
						// he is between the two switchs. In this case there is a collision...
						collision = true;
					}

					if (!collision) {
						desiredLane = lane;
						break;
					}
				}

				if (desiredLane < WorldModel.Self.LaneIndex) PolitelySwitchLanes(-1);
				else if (desiredLane > WorldModel.Self.LaneIndex) PolitelySwitchLanes(1);
			}
		}

		// Turbo to fuck people
		if (!WorldModel.willSwitchLanes
		    && WorldModel.existsVeryCloseEnemy
		    && WorldModel.turboAvailable
		    && WorldModel.Race.Track.Pieces[idx].Radius == 0
		    && WorldModel.Race.Track.Pieces[(idx + 1)%piecesCount].Radius == 0) 
		{
			Console.WriteLine ("XUPADAWAN TURBO");
			pleaseActivateTheTurbo = true;
		}

		// Check Turbo

//		if (!WorldModel.willSwitchLanes
//		    && WorldModel.turboAvailable 
//		    && WorldModel.Self.PiecePosition.Lap > 1 
//		    && WorldModel.Self.PiecePosition.PieceIndex > piecesCount*0.75) 
//		{
//			double straightLength = 0;
//			int i = idx;
//			if (Math.Abs(WorldModel.Self.Angle) < 5 ) if (pieces [i].Radius > 0) i = (idx + 1) % piecesCount;;
//			if (Math.Abs(WorldModel.Self.Angle) < 20 ) if (pieces [i].Radius > 0) i = (idx + 1) % piecesCount;;
//			while (true) {
//				if (pieces [i].Radius > 0) break;
//				straightLength += pieces [i].Length;
//				i = (i + 1) % pieces.Count;
//			}
//
//			if (straightLength > 250) pleaseActivateTheTurbo = true;
//		}

		t = findThrottle();

		// Comment to enable turbo in all situations where we want higher throttle
		//if (!WorldModel.existsVeryCloseEnemy && WorldModel.TurboActive)	t /= WorldModel.activeTurboFactor;

		// Update state and send throttle
		Logger.Aggregate("Throttle", t);
		ThrottleModel.updateThrottle(t);
        
        if (!Logger.IsPrintingMatrix) Logger.LogMatrixHeader("selfData");
        Logger.FlushAggregator();

		return t/(WorldModel.TurboActive ? WorldModel.activeTurboFactor : 1.0);
	}

	// Between 0 and maxThrottle (1 or turbo)
	public double findThrottle ()
	{
		bool usingTurbo = WorldModel.WillTurboBeActive (WorldModel.currentGameTick);

		double t = 0.0;

		var pieces = WorldModel.Race.Track.Pieces;
		int piecesCount = pieces.Count;

		Piece nextCurve = pieces [(WorldModel.Self.LaneIndex + 1) % piecesCount];

		double targetSpeed = 
			SpeedModel.speedPerPiecePerLane [WorldModel.Self.PiecePosition.PieceIndex] [WorldModel.Self.PiecePosition.Lane.EndLaneIndex];
		double nextTargetSpeed = 
			SpeedModel.speedPerPiecePerLane [(WorldModel.Self.PiecePosition.PieceIndex + 1) % WorldModel.Race.Track.Pieces.Count] [WorldModel.Self.PiecePosition.Lane.EndLaneIndex];

		if (WorldModel.CurrentPiece.Radius > 0) {
			if (nextTargetSpeed < targetSpeed) {
				if(!WorldModel.canMantainSpeed(targetSpeed)){ // can get to next piece in its speed
					targetSpeed = nextTargetSpeed;
				}
			}
			else {
				// angle is decreasing
				if(Math.Sign(AngleModel.dangles[WorldModel.currentGameTick])!=
				   Math.Sign(AngleModel.angles[WorldModel.currentGameTick]) && 
				   Math.Sign(AngleModel.angles[WorldModel.currentGameTick])==Math.Sign(WorldModel.CurrentPiece.Angle) &&
				   Math.Abs (AngleModel.angles[WorldModel.currentGameTick])<55){
					Console.WriteLine("ACELERANDO");
					if(WorldModel.canMantainSpeed(nextTargetSpeed*1.1)){
						targetSpeed = nextTargetSpeed*1.1;
					}
					else {
						targetSpeed = nextTargetSpeed;
					}
				}
			}

		} else {
			if(!WorldModel.canMantainSpeed(targetSpeed)){ // can get to next piece in its speed
				targetSpeed = nextTargetSpeed;
			}
		}
		if (AngleModel.isDangerous ()) { // We are almost losing control
			if (WorldModel.CurrentPiece.Radius > 0) {
				double acp = 0.3;
				targetSpeed = Math.Sqrt (acp * WorldModel.CurrentPiece.Radius);
			} else {
				targetSpeed = 6;
			}
		}

		if (targetSpeed == 0) { // weird... that shouldnt happen
			Console.WriteLine("[WARNING] Ignoring speed 0");
			targetSpeed = 1000;
		}

		//Console.WriteLine ("Speed = {0}", targetSpeed);

		// Long straight piece
		if (targetSpeed>10 && WorldModel.turboAvailable) {
			bool ehRetao = true;
			for(int i = 0; i < 4; i++){
				if(SpeedModel.speedPerPiecePerLane [(WorldModel.Self.PiecePosition.PieceIndex + i) % WorldModel.Race.Track.Pieces.Count] [WorldModel.Self.PiecePosition.Lane.EndLaneIndex] < 10){
					ehRetao = false;
				}
			}
			if(ehRetao){
				pleaseActivateTheTurbo=true;
			}
		}
		if (targetSpeed > 13)
			targetSpeed = 13;
		if (WorldModel.speeds [WorldModel.currentGameTick] < targetSpeed - 0.02) {
			t = double.PositiveInfinity;
		} else if (WorldModel.speeds [WorldModel.currentGameTick] < targetSpeed + 0.01) {
			t = ThrottleModel.GetThrottleFromSpeed (targetSpeed, usingTurbo ? WorldModel.activeTurboFactor : 1.0);
		} else {
			t = 0.0;
		}

		return t < (usingTurbo ? WorldModel.activeTurboFactor : 1.0)
			   ? t
			   : (usingTurbo ? WorldModel.activeTurboFactor : 1.0);
	}

	public static void PolitelySwitchLanes(int targetIndexChange) {
		WorldModel.willSwitchLanes = true;
		pleaseSwitch = targetIndexChange;
	}
}
