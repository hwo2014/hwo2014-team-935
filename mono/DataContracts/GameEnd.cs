using System;
using System.Collections.Generic;
using Newtonsoft.Json;

public class GameEnd
{
	[JsonProperty("results")]
	public List<CarResult> Results { get; private set; }

	[JsonProperty("bestLaps")]
	public List<BestLaps> BestLaps { get; private set; }
}

public class CarResult
{
    [JsonProperty("car")]
    public Car Car { get; private set; }

    [JsonProperty("result")]
    public RaceTime Result { get; private set; }
}

public class BestLaps
{
    [JsonProperty("car")]
    public Car Car { get; private set; }

    [JsonProperty("result")]
    public LapTime Result { get; private set; }
}
