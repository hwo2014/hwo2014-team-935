using System;
using Newtonsoft.Json;

public class Piece
{
	[JsonProperty("length")]
	public double Length { get; private set; }

	[JsonProperty("switch")]
	public bool Switch { get; private set; }

	[JsonProperty("angle")]
	public double Angle { get; private set; }

	[JsonProperty("radius")]
	public double Radius { get; private set; }

    public double RealRadius(int lane)
    {
        if (Radius == 0) return 0;

		return Radius - Math.Sign(Angle) * WorldModel.Race.Track.Lanes[lane].DistanceFromCenter;
    }

    public double RealLength(int lane)
    {
        if (Length > 0) return Length;

        double radians = Angle*Math.PI/180f;
        return Math.Abs(radians)*(RealRadius(lane));
    }
}
