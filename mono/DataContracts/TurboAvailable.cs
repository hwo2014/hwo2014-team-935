using System;
using Newtonsoft.Json;

public class TurboAvailable
{
    [JsonProperty("turboDurationMilliseconds")]
    public double TurboDurationMilliseconds { get; private set; }

    [JsonProperty("TurboDurationTicks")]
    public int TurboDurationTicks { get; private set; }

    [JsonProperty("turboFactor")]
    public double TurboFactor { get; private set; }
}
