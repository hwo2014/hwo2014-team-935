using System;
using Newtonsoft.Json;

public class Position 
{
	[JsonProperty("x")]
	public double X { get; private set; }
	
	[JsonProperty("y")]
	public double Y { get; private set; }
}

