using System;
using Newtonsoft.Json;

public class Dimensions
{
	[JsonProperty("length")]
	public double Length { get; private set; }

	[JsonProperty("width")]
	public double Width { get; private set; }

	[JsonProperty("guideFlagPosition")]
	public double GuideFlagPosition { get; private set; }
}

public class CarDescription
{
	[JsonProperty("id")]
	public Car Id { get; private set; }

	[JsonProperty("dimensions")]
	public Dimensions Dimensions { get; private set; }
}

