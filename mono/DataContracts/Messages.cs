﻿using System;
using Newtonsoft.Json;

class MsgWrapper
{
    public string msgType;
    public Object data;
    public int gameTick;
    public string gameId;

    public MsgWrapper(string msgType, Object data, int gameTick, string gameId = null)
    {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
        this.gameId = gameId;
    }
}

class TickableMsgWrapper
{
	public string msgType;
	public Object data;
	public int gameTick;

	public TickableMsgWrapper(string msgType, Object data, int gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}
}

class NonTickableMsgWrapper
{
	public string msgType;
	public Object data;

	public NonTickableMsgWrapper(string msgType, Object data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}

abstract class SendMsg
{
    public string ToJson()
    {
		int tick = this.GameTick();
		if (tick == -1)	return JsonConvert.SerializeObject (new NonTickableMsgWrapper (this.MsgType (), this.MsgData ()));
		return JsonConvert.SerializeObject(new TickableMsgWrapper(this.MsgType(), this.MsgData(), this.GameTick()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected virtual int GameTick()
    {
        return -1;
    }

    protected abstract string MsgType();
}

class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Throttle : SendMsg
{
    public double value;
    public int gameTick;

    public Throttle(double value, int gameTick)
    {
        this.value = value;
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }

    protected override int GameTick()
    {
        return gameTick;
    }
}

enum Direction { Left, Right }

class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(Direction value)
    {
        this.value = value.ToString();
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

class EnableTurbo : SendMsg
{
    protected override Object MsgData()
    {
        return "XUPADAWAN!";
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}

class CreateRace : SendMsg
{
    public BotID botId;
    public string trackName;
    public string password;
    public int carCount;

    public CreateRace(BotID botId, string trackName, string password, int carCount)
    {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "createRace";
    }
}

public class BotID
{
    public string name;
    public string key;

    public BotID(string name, string key)
    {
        this.name = name;
        this.key = key;
    }
}

class JoinRace : CreateRace
{
    public JoinRace(BotID botId, string trackName, string password, int carCount) : base(botId, trackName, password, carCount) { }

    protected override string MsgType()
    {
        return "joinRace";
    }
}