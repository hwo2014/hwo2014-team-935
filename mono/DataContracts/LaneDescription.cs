using System;
using Newtonsoft.Json;

public class LaneDescription
{
	[JsonProperty("distanceFromCenter")]
	public double DistanceFromCenter { get; private set; }

	[JsonProperty("index")]
	public int Index { get; private set; }

}

