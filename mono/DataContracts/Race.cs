using System;
using Newtonsoft.Json;
using System.Collections.Generic;


public class Race
{
	[JsonProperty("track")]
	public Track Track { get; private set; }
	
	[JsonProperty("cars")]
	public List<CarDescription> Cars { get; private set; }

	[JsonProperty("raceSession")]
	public RaceSession RaceSession { get; private set; }
}


