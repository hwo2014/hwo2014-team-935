using System;
using Newtonsoft.Json;

public class PiecePosition
{
	[JsonProperty("pieceIndex")]
	public int PieceIndex { get; private set; }

	[JsonProperty("inPieceDistance")]
	public double InPieceDistance { get; private set; }

	[JsonProperty("lane")]
	public Lane Lane { get; private set; }

	[JsonProperty("lap")]
	public int Lap { get; private set; }
}
