using System;
using Newtonsoft.Json;

public class StartingPoint
{
	[JsonProperty("position")]
	public Position Position { get; private set; }

	[JsonProperty("angle")]
	public double Angle { get; private set; }
}

