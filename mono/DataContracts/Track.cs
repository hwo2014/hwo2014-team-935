using System;
using Newtonsoft.Json;
using System.Collections.Generic;

public class Track
{
	[JsonProperty("id")]
	public string Id { get; private set; }

	[JsonProperty("name")]
	public string Name { get; private set; }

	[JsonProperty("pieces")]
	public List<Piece> Pieces { get; private set; }

	[JsonProperty("lanes")]
	public List<LaneDescription> Lanes { get; private set; }

	[JsonProperty("startingPoint")]
	public StartingPoint StartingPoint { get; private set; }

	public Tuple<int, double> getPiecePosition(PiecePosition start, double length) {
		int lane = start.Lane.EndLaneIndex;

		if (start.InPieceDistance + length < Pieces [start.PieceIndex].RealLength (lane)) {
			return new Tuple<int, double> (start.PieceIndex, start.InPieceDistance + length);
		}

		length -= Pieces[start.PieceIndex].RealLength(lane) - start.InPieceDistance;
		
		int piecesCount = Pieces.Count;
		int currentIndex = (start.PieceIndex + 1) % piecesCount;

		while (true) {
			if (length < Pieces[currentIndex].RealLength(lane)) {
				return new Tuple<int, double>(currentIndex, length);
			}

			length -= Pieces[currentIndex].RealLength(lane);
			currentIndex = (currentIndex + 1) % piecesCount;
		}
	}

	public double DistanceAtoB(PiecePosition A, PiecePosition B, int? lane = null) {
		if (lane == null && A.Lane.EndLaneIndex != B.Lane.EndLaneIndex) throw new InvalidOperationException("Pass a custom lane or use piece positions in the same lane");

		if (A.PieceIndex == B.PieceIndex && A.InPieceDistance <= B.InPieceDistance) 
		{
			return B.InPieceDistance - A.InPieceDistance;
		}

		int currIdx = (A.PieceIndex + 1) % Pieces.Count;
		double distance = Pieces[A.PieceIndex].RealLength(lane ?? A.Lane.EndLaneIndex) - A.InPieceDistance;

		while (currIdx != B.PieceIndex) {
			distance += Pieces [currIdx].RealLength (lane ?? A.Lane.EndLaneIndex);
			currIdx = (currIdx + 1) % Pieces.Count; 
		}

		return distance + B.InPieceDistance;
	}

	// If lanes are not the same, then assume A is on the B lane
	public bool IsAAheadOfB(PiecePosition A, PiecePosition B, int? lane = null) {
		return DistanceAtoB (A, B, lane) > DistanceAtoB (B, A, lane);
	}

	public bool IsPieceABetweenBandC(int A, int B, int C) {
		if (A < B && C < B)	return A < C;

		if (A < B) A += Pieces.Count;
		if (C < B) C += Pieces.Count;
		return A > B && A < C;
	}
}

