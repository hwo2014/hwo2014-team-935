using System;
using Newtonsoft.Json;

public class Car
{
	[JsonProperty("name")]
	public string Name { get; private set; }

	[JsonProperty("color")]
	public string Color { get; private set; }
}
