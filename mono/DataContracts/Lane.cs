using System;
using Newtonsoft.Json;

public class Lane
{
	[JsonProperty("startLaneIndex")]
	public int StartLaneIndex { get; private set; }

	[JsonProperty("endLaneIndex")]
	public int EndLaneIndex { get; private set; }
}
