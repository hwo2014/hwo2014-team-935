using System;
using Newtonsoft.Json;

public class LapFinished
{
	[JsonProperty("car")]
	public Car Car { get; private set; }

	[JsonProperty("lapTime")]
    public LapTime LapTime { get; private set; }

    [JsonProperty("raceTime")]
    public RaceTime RaceTime { get; private set; }

    [JsonProperty("ranking")]
    public Ranking Ranking { get; private set; }
}

public class LapTime
{
    [JsonProperty("lap")]
    public int Lap { get; private set; }

    [JsonProperty("ticks")]
    public int Ticks { get; private set; }

    [JsonProperty("millis")]
    public int Millis { get; private set; }
}

public class RaceTime
{
    [JsonProperty("laps")]
    public int Laps { get; private set; }

    [JsonProperty("ticks")]
    public int Ticks { get; private set; }

    [JsonProperty("millis")]
    public int Millis { get; private set; }
}

public class Ranking
{
    [JsonProperty("overall")]
    public int Overall { get; private set; }

    [JsonProperty("fastestLap")]
    public int FastestLap { get; private set; }
}