using System;
using Newtonsoft.Json;

public class CarPosition
{
	[JsonProperty("id")]
	public Car Car { get; private set; }

	public string Name { get { return Car.Name; } }
	public string Color { get { return Car.Color; } }

    public int LaneIndex { get { return PiecePosition.Lane.EndLaneIndex;  } }

	[JsonProperty("angle")]
	public double Angle { get; private set; }

	[JsonProperty("piecePosition")]
	public PiecePosition PiecePosition { get; private set; }
}
