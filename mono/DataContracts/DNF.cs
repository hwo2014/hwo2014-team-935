using System;
using Newtonsoft.Json;

public class DNF
{
	[JsonProperty("car")]
	public Car Car { get; private set; }

	[JsonProperty("reason")]
	public string Reason { get; private set; }
}
