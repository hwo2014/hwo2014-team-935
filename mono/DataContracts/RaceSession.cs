using System;
using Newtonsoft.Json;

public class RaceSession
{
	[JsonProperty("laps")]
	public int Laps { get; private set; }

	[JsonProperty("maxLapTimeMs")]
	public int MaxLapTimeMs { get; private set; }

	[JsonProperty("quickRace")]
	public bool QuickRace { get; private set; }

    [JsonProperty("durationMs")]
    public int DurationMs { get; private set; }
}


