﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

static class Logger
{
    public static bool IsPrintingMatrix { get; private set; }
    static LinkedList<Tuple<string, object>> fields = new LinkedList<Tuple<string, object>>();

    public static void Log(object message)
    {
        Console.WriteLine(message);
    }

    public static void Aggregate(string name, object field)
    {
        fields.AddLast(new Tuple<string, object>(name, field));
    }

    public static void FlushAggregator()
    {

        Console.Write(String.Join(",\t", fields.Select(t => t.Item2 is double ? ((double) t.Item2).ToString("0.00") : t.Item2.ToString()).ToArray()));
        Console.WriteLine(";");
		 
        fields.Clear();

    }

    public static void LogMatrixHeader(string matrixName)
    {
        IsPrintingMatrix = true;
        Console.Write("Matrix Schema: [");
        Console.Write(String.Join(",\t", fields.Select(t => t.Item1).ToArray()));
        Console.WriteLine("]");
        Console.Write(matrixName + "= [");
    }

    public static void LogMatrixEnd()
    {
        Console.WriteLine("]");
        IsPrintingMatrix = false;
    }

	public static void LogRace()
	{
		Console.WriteLine("Track {0}  {1}", WorldModel.Race.Track.Id, WorldModel.Race.Track.Name);
		foreach(var x in WorldModel.Race.Track.Pieces){
			Console.WriteLine("Piece: {0} {1} {2} {3}", 
			                  x.Length, 
			                  x.Switch, 
			                  x.Radius, 
			                  x.Angle);
		}
		foreach(var x in WorldModel.Race.Track.Lanes){
			Console.WriteLine("Lane: {0} {1}", x.DistanceFromCenter, x.Index);
		}
		
		Console.WriteLine("Cars");
		foreach(var x in WorldModel.Race.Cars){
			Console.WriteLine("Car: {0} {1} {2} {3} {4}",
			                  x.Id.Name,
			                  x.Id.Color,
			                  x.Dimensions.Length,
			                  x.Dimensions.Width,
			                  x.Dimensions.GuideFlagPosition);
		}
		Console.WriteLine("race Session: {0} {1} {2}", 
		                  WorldModel.Race.RaceSession.Laps,
		                  WorldModel.Race.RaceSession.MaxLapTimeMs,
		                  WorldModel.Race.RaceSession.QuickRace);
	}

}
