using System;
using System.Collections.Generic;


public static class DictionaryExtensions
{
	public static V Get<K, V>(this IDictionary<K, V> dic, K key, V defaultValue)
	{
		V value;
		return dic.TryGetValue(key, out value) ? value : defaultValue;
	}
}

