using System;
using System.Linq;
using System.Collections.Generic;

public static class ThrottleModel
{
	// Over time (gameTicks)
	public static Dictionary<int, double> throttles = new Dictionary<int, double>();
	
	public static double airDrag = -1;
	public static double pedal = -1;
	public static bool KnownCoefficients { get { return airDrag != -1 && pedal != -1; } }
	
	public static double NextSpeed(double throttle, double? speed = null)
	{
		if (!KnownCoefficients) return -1;
		return (speed ?? WorldModel.speeds[WorldModel.currentGameTick]) * airDrag + pedal * throttle;
	}

    public static double GetThrottleFromSpeed(double speed, double turboFactor = 1)
    {
        if (!KnownCoefficients) return -1.0;
        double t = speed * (1.0 - airDrag) / pedal;
        return t < turboFactor ? t : turboFactor;
    }
	
	public static void updateModel ()
	{
		if (!KnownCoefficients)
		{
			for (int i = 0; i < WorldModel.currentGameTick; ++i)
			{
				if (!KnownCoefficients && WorldModel.speeds[i] > 0)
				{
					pedal = WorldModel.speeds[i] / throttles[i - 1];
					airDrag = (WorldModel.speeds[i + 1] - pedal * throttles[i]) / WorldModel.speeds[i];
					WorldModel.onThrottleDiscovered();
				}
			}
		}
		//Console.WriteLine("Pedal {0} Airdrag {1}",pedal,airDrag);
	}

	public static void updateThrottle (double t)
	{
		throttles[WorldModel.currentGameTick] = t;
	}
	
	public static Tuple<int, double> GetFuturePosition (IEnumerable<double> inputThrottles) {
		if (!KnownCoefficients) return new Tuple<int, double> (-1, -1);
		if (inputThrottles == null)	throw new ArgumentNullException("inputThrottles");

		var speeds = new LinkedList<double> ();

		double speed = WorldModel.speeds[WorldModel.currentGameTick];
		foreach (var t in inputThrottles) {
			speed = NextSpeed(t, speed);
			speeds.AddLast (speed);
		}

		return GetFuturePosition (speeds);
	}

	public static Tuple<int, double> GetFuturePosition (IEnumerable<double> speeds, string color = null) {
		if (speeds == null)	throw new ArgumentNullException("speeds");

		PiecePosition pos;
		if (color == null) pos = WorldModel.Self.PiecePosition;
		else pos = WorldModel.Enemies.Find (e => e.Color == color).PiecePosition;

		return WorldModel.Race.Track.getPiecePosition(pos, speeds.Sum());
	}

	public static Tuple<int, double> GetFuturePosition (double speed, int cycles, string color = null) {
		return GetFuturePosition(Enumerable.Repeat(speed, cycles), color);
	}

}
