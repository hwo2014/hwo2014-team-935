using System;

public class CarModel
{
	public Car Car;
	public PiecePosition PiecePosition;
	public Dimensions Dimensions;
	public double Angle { get; private set; }

	public double speed;
	public int lastPieceIndex;
	public double lastInPieceDistance;

	public void updateCarModel(PiecePosition piecePosition) {
		if (WorldModel.currentGameTick == 0) {
			speed = 0;
			PiecePosition = piecePosition;
		} else {
			lastPieceIndex = PieceIndex;
			lastInPieceDistance = InPieceDistance;
			PiecePosition = piecePosition;

			// Should be extracted to a function (also used in the self model)
			speed = InPieceDistance >= lastInPieceDistance
				? (InPieceDistance - lastInPieceDistance)
					: WorldModel.Race.Track.Pieces[lastPieceIndex].RealLength(LaneIndex) - lastInPieceDistance + InPieceDistance;
		}
	}

	public string Name { get { return Car.Name; } }
	public string Color { get { return Car.Color; } }
	public int LaneIndex { get { return PiecePosition.Lane.EndLaneIndex;  } }
	public int Lap { get { return PiecePosition.Lap; } }
	public int PieceIndex { get { return PiecePosition.PieceIndex; } }
	public double InPieceDistance { get { return PiecePosition.InPieceDistance; } }
	public int EndLaneIndex { get { return PiecePosition.Lane.EndLaneIndex; } }
	public int StartLaneIndex { get { return PiecePosition.Lane.StartLaneIndex; } }
	public double Length { get { return Dimensions.Length; } }
	public double Width { get { return Dimensions.Width; } }
	public double GuideFlagPosition { get { return Dimensions.GuideFlagPosition; } }

}
