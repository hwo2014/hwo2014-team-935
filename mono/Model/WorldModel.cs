using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;



public static class WorldModel
{
	public static CarPosition Self;
	public static List<CarModel> Enemies = new List<CarModel>();
	public static Race Race;
	public static int currentGameTick;
	public static Piece CurrentPiece { get { return Race.Track.Pieces[pieceIndexes[currentGameTick]]; } }
	public static bool turboAvailable = false;
	public static int turboDurationTicks = 0;
	public static int activeTurboDurationTicks = 0;
	public static double turboFactor = 0;
	public static double activeTurboFactor = 0;
	public static int turboActivationTick = Int32.MinValue;
	public static bool TurboActive { get { return WillTurboBeActive (currentGameTick); } }
	public static bool willSwitchLanes = false;
	public static bool existsVeryCloseEnemy = false;


	public static Dictionary<int, double> distances = new Dictionary<int, double>();
	public static Dictionary<int, double> speeds = new Dictionary<int, double>();
	public static Dictionary<int, int> pieceIndexes = new Dictionary<int, int>();
	public static Dictionary<int, double> inPieceDistances = new Dictionary<int, double>();
	public static Dictionary<int, int> laneIndexes = new Dictionary<int, int>();

	public static void updateModel ()
	{
		pieceIndexes [currentGameTick] = Self.PiecePosition.PieceIndex;
		inPieceDistances [currentGameTick] = Self.PiecePosition.InPieceDistance;
		laneIndexes [currentGameTick] = Self.LaneIndex;

		if (currentGameTick == 0) {
			speeds [currentGameTick] = 0;
		} else {
			if (pieceIndexes [currentGameTick] != pieceIndexes [currentGameTick - 1]) willSwitchLanes = false;

			// Should be extracted to a function (also used in the enemy model)
			speeds [currentGameTick] = inPieceDistances [currentGameTick] >= inPieceDistances.Get (currentGameTick - 1, 0)
				? (inPieceDistances [currentGameTick] - inPieceDistances.Get (currentGameTick - 1, 0)) 
				: Race.Track.Pieces [pieceIndexes.Get (currentGameTick - 1, 0)].RealLength (laneIndexes.Get (currentGameTick - 1, 0))
					- inPieceDistances.Get (currentGameTick - 1, 0) + inPieceDistances [currentGameTick];
		}
		
		distances[currentGameTick] = distances.Get(currentGameTick - 1, 0) + speeds[currentGameTick];

		existsVeryCloseEnemy = Enemies.Find (
				e => WorldModel.Self.LaneIndex == e.LaneIndex 
				&& WorldModel.Race.Track.DistanceAtoB (WorldModel.Self.PiecePosition, e.PiecePosition) < 2 * e.Length)
			!= null;
	}
	
	public static bool WillTurboBeActive (int tick)
	{
		return turboActivationTick + activeTurboDurationTicks >= tick;
	}

	public static void createThreadToAjustSpeeds ()
	{
		Thread thread = new Thread(new ThreadStart(SpeedModel.adjustSpeeds));	
		thread.Priority = ThreadPriority.Lowest;
		thread.Start();
	}

	public static void onThrottleDiscovered(){
		createThreadToAjustSpeeds();
	}

	public static bool canMantainSpeed (double speed)
	{
		double pos = Self.PiecePosition.InPieceDistance;
		pos += speed;
		while (pos<CurrentPiece.RealLength(Self.LaneIndex)) {
			pos += speed;
			speed *= ThrottleModel.airDrag;
			if (speed < 2)
				break;
		}
		if (speed > SpeedModel.speedPerPiecePerLane [nextPieceId (Self.PiecePosition.PieceIndex)] [Self.LaneIndex]) {
			return false;
		}
		while (pos<CurrentPiece.RealLength(Self.LaneIndex)+Race.Track.Pieces[nextPieceId (Self.PiecePosition.PieceIndex)].RealLength(Self.LaneIndex)) {
			pos += speed;
			speed *= ThrottleModel.airDrag;
			if (speed < 2)
				break;
		}
		if (speed > SpeedModel.speedPerPiecePerLane [nextPieceId (nextPieceId(Self.PiecePosition.PieceIndex))] [Self.LaneIndex]) {
			return false;
		}
		return true;
	}

	public static int nextPieceId(int id){
		return (id+1)%Race.Track.Pieces.Count;
	}

	public static int previousPieceId(int id){
		return (id-1+Race.Track.Pieces.Count)%Race.Track.Pieces.Count;
	}
}
