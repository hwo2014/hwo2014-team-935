using System;
using System.Collections.Generic;
using MathNet.Numerics;

public static class AngleModel
{
	public static Dictionary<int, double> angles = new Dictionary<int, double>();
	public static Dictionary<int, double> dangles = new Dictionary<int, double>();
	public static Dictionary<int, double> d2angles = new Dictionary<int, double>();

	public static double lastAngle = 0;
	public static double lastdAngle = 0;

	public static double kp=1,kpd=10.3,kpd2=111.93,q1=500,q2=-136.37; // Magic numbers... see matlab :p

	// Simulating diferential equation
	public static Tuple<double,double,double> NextState (double a, double da, double d2a, double acp, int dir)
	{
		double a1 = a + da;
		double da1 = da + d2a;
		double u = q1 * acp + q2;
		if (u < 0) {
			u=0;
		}
		double d2a1 = (dir*u - (kp*a+kpd*da))/kpd2;
		return Tuple.Create(a1,da1,d2a1);
	}

	public static Tuple<double,double,double> NextPieceState (int idx, double speed, double pos, int lane, double a, double da, double d2a)
	{
		// Ctr c Ctr v da funcao abaixo com algumas modificacoes :p
		double acp=0;
		for (int i = 0; i<400; i++) {
			int dir = Math.Sign(WorldModel.Race.Track.Pieces[idx].Angle);
			double r = WorldModel.Race.Track.Pieces[idx].Radius;
			if(r>0){
				acp = speed*speed/r;
				speed*=(1-(1-ThrottleModel.airDrag)/2); // I can't brake a lot in curv
			}
			else{ 
				acp=0;
				speed*=ThrottleModel.airDrag; // I can brake in straight line
			}
			var t = NextState(a,da,d2a,acp,dir);
			a=t.Item1;
			da=t.Item2;
			d2a=t.Item3;

			pos+=speed;
			if(pos>WorldModel.Race.Track.Pieces[idx].RealLength(lane)){
				return Tuple.Create(a,da,d2a);
			}
		}

		return Tuple.Create(0.0,0.0,0.0);
	}

	public static void updateModel ()
	{
		double lastAngle = WorldModel.currentGameTick == 0 ? 0 : angles [WorldModel.currentGameTick - 1];
		double lastdAngle = WorldModel.currentGameTick == 0 ? 0 : dangles [WorldModel.currentGameTick - 1];
		angles [WorldModel.currentGameTick] = WorldModel.Self.Angle;
		dangles [WorldModel.currentGameTick] = WorldModel.Self.Angle - lastAngle;
		d2angles [WorldModel.currentGameTick] = dangles [WorldModel.currentGameTick] - lastdAngle;
	}

	public static bool isStable (int curvIdx, double pos, double speed, double a=0, double da=0, double d2a=0, int lane = -1)
	{
		// Default lane
		if (lane == -1) {
			lane = WorldModel.Self.LaneIndex;
		}
		double acp = 0;
		for (int i = 0; i<500; i++) {
			int dir = Math.Sign(WorldModel.Race.Track.Pieces[curvIdx].Angle);
			double r = WorldModel.Race.Track.Pieces[curvIdx].Radius;
			if(r>0){
				acp = speed*speed/r;
				speed*=(1-(1-ThrottleModel.airDrag)/2); // I can't brake a lot in curv
			}
			else{ 
				acp=0;
				speed*=(1-(1-ThrottleModel.airDrag)*2/3.0); // I can brake in straight line
			}
			if(acp>0.5){ // too fast
				return false;
			}
			var t = NextState(a,da,d2a,acp,dir); // Runge Kutta!!! :)
			a=t.Item1;
			da=t.Item2;
			d2a=t.Item3;

			// dangerous
			// ps.: eu podia usar a funcao abaixo pra isso mas eu fiz essa condicao antes
			// e ta funcionando assim... nao se mexe em time q ta ganhando ne
			if((Math.Abs(a)>42 && Math.Sign(a) == Math.Sign(da)) || Math.Abs(a) > 50)return false;

			// Simulating movement
			pos+=speed;
			if(pos>WorldModel.Race.Track.Pieces[curvIdx].RealLength(lane)){
				pos-=WorldModel.Race.Track.Pieces[curvIdx].Length;
				curvIdx++;
				curvIdx = curvIdx%WorldModel.Race.Track.Pieces.Count;
			}
		}
		return true;
	
	}

	// Tells if the car is almost losing control
	public static bool isDangerous (double a = 500, double da = 500, double d2a = 500)
	{
		if (a == 500) {
			a = angles [WorldModel.currentGameTick];
			da = dangles [WorldModel.currentGameTick];
			d2a = d2angles [WorldModel.currentGameTick];
		}
		if (Math.Sign (da) == Math.Sign (a)) {
			List<double> angs = new List<double>{41,45,50,54};
			List<double> dangs = new List<double>{2.5,1,0.6,0};
			for(int i=0;i<angs.Count;i++){
				if(Math.Abs(a) > angs[i] && Math.Abs (da)>dangs[i]){
					return true;
				}
			}
		}
		return Math.Abs(a) > 55;
	}

}
