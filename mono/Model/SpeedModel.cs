using System;
using System.Collections.Generic;

public static class SpeedModel
{
	public static Dictionary<int,Dictionary<int, double>> speedPerPiecePerLane = new Dictionary<int,Dictionary<int, double>>();

	// factor speed was multiplied in last lap
	public static Dictionary<int,Dictionary<int, double>> updated = new Dictionary<int,Dictionary<int, double>>();

	public static readonly double initialAcp = 0.42;

	public static void initialize ()
	{
		lastPieceIndex = -1;
		if(speedPerPiecePerLane.Count!=0)return; // trying to initialize in the actual race
		var pieces = WorldModel.Race.Track.Pieces;
		for(int i=0;i<pieces.Count;i++){
			speedPerPiecePerLane[i] = new Dictionary<int, double>();
			updated[i] = new Dictionary<int, double>();
			for(int lane = 0; lane<WorldModel.Race.Track.Lanes.Count;lane++){
				updated[i][lane]=1;
				if(pieces[i].Radius>0){
					speedPerPiecePerLane[i][lane]=Math.Sqrt(initialAcp*pieces[i].RealRadius(lane));
				}
				else{
					speedPerPiecePerLane[i][lane]=20;
				}
			}
		}
	}

	public static int lastPieceIndex = -1;
	public static double maxAngle;

	public static void multSpeed (int index, int lane, double factor)
	{
		if (updated [index][lane]==1 || factor < updated [index][lane]) {
			Console.WriteLine("Multiplicando index {0} lane {1} por  {2:0.00}",index,lane,factor);
			speedPerPiecePerLane[index][lane] *= factor/updated [index][lane];
			updated[index][lane] = factor;
		}
	}

	public static void update ()
	{
		if (lastPieceIndex == WorldModel.Race.Track.Pieces.Count - 1 && WorldModel.Self.PiecePosition.PieceIndex == 0) {
			var pieces = WorldModel.Race.Track.Pieces;
			bool changed = false;
			for(int i=0;i<pieces.Count;i++){
				for(int lane = 0; lane<WorldModel.Race.Track.Lanes.Count;lane++){
					if(updated[i][lane]!=1)changed=true;
					updated[i][lane]=1;
				}
			}
			if(changed){
				WorldModel.createThreadToAjustSpeeds(); // diminuir nas retas pra entrar nas curvas
			}
		}
		if (lastPieceIndex != WorldModel.Self.PiecePosition.PieceIndex) {
			if(lastPieceIndex!=-1){
				for(int i =0;i<WorldModel.Race.Track.Lanes.Count;i++){
					if(WorldModel.Race.Track.Pieces[lastPieceIndex].Radius>0){
						double factor = 1;
						if (maxAngle == 0) {
							factor = 1.001; // pode ter sido isolado no piece anterior
							// se ele foi isolado no lane anterior factor deveria ser 1
							// se ele ta lento mesmo ai ele vai continuar lento mas espera-se
							// que na proxima volta ele atinja um angulo maior que zero e entre
							// nos casos abaixo
						} 
						else if (maxAngle < 10) {
							factor = 1.11;
						} else if (maxAngle < 27) {
							factor = 1.055;
						} else if (maxAngle < 39) {
							factor = 1.025;
						} else if (maxAngle < 45) {
							factor = 1.015;
						} else if (maxAngle > 56) {
							factor = 0.83;
						} else if (maxAngle > 55) {
							factor = 0.95;
						} else if (maxAngle > 54) {
							factor = 0.97;
						} else if (maxAngle > 53) {
							factor = 0.98;
						} else if (maxAngle > 52) {
							factor = 0.99;
						}


						multSpeed(lastPieceIndex,i,factor);

						// previous pieces
						int previousIndex = (WorldModel.Race.Track.Pieces.Count+lastPieceIndex-1 + WorldModel.Race.Track.Pieces.Count)
							%WorldModel.Race.Track.Pieces.Count;
						int beforePreviousIndex = (WorldModel.Race.Track.Pieces.Count+lastPieceIndex-2 + WorldModel.Race.Track.Pieces.Count)
							%WorldModel.Race.Track.Pieces.Count;
						if(factor <= 1){
							multSpeed(previousIndex,i,factor);
							multSpeed(beforePreviousIndex,i,factor);
						}else {
							if(WorldModel.Race.Track.Pieces[previousIndex].Radius==0){
								multSpeed(previousIndex,i,factor);
								if(WorldModel.Race.Track.Pieces[beforePreviousIndex].Radius==0){
									multSpeed(beforePreviousIndex,i,factor);
								}
							}
						}

						double acp = Math.Pow(speedPerPiecePerLane[lastPieceIndex][i],2)/WorldModel.Race.Track.Pieces[lastPieceIndex].Radius;
						if(acp>0.5){
							acp = 0.5;
						}
						speedPerPiecePerLane[lastPieceIndex][i]=Math.Sqrt(acp * WorldModel.Race.Track.Pieces[lastPieceIndex].Radius);
					}
				}
			}
			lastPieceIndex = WorldModel.Self.PiecePosition.PieceIndex;
			maxAngle = Math.Abs(AngleModel.angles [WorldModel.currentGameTick]);
		} else {
			maxAngle=Math.Max(maxAngle,Math.Abs(AngleModel.angles [WorldModel.currentGameTick]));
		}
	}


	public static void adjustSpeeds ()
	{
		var pieces = WorldModel.Race.Track.Pieces;
		int retry = 10;
		for (int t = 0; t<retry; t++) {
			bool changed = false;
			for (int i=0; i<pieces.Count; i++) {
				for (int lane = 0; lane<WorldModel.Race.Track.Lanes.Count; lane++) {
					double v0 = speedPerPiecePerLane [i] [lane];
					double d = WorldModel.Race.Track.Pieces [i].RealLength (lane);
					double airdrag = ThrottleModel.airDrag;
					double speed = speedPerPiecePerLane [(i + 1) % pieces.Count] [lane];
					double pos = d;
					while (pos>0) {
						pos -= speed;
						speed /= airdrag;
					}
					// 0.98 is the safety factor
					speed *= 0.98;
					// speed is how fast I can be considering the next piece
					if(WorldModel.Race.Track.Pieces[i].Radius==0 || v0 > speed){
						changed = true;
						v0 = speed; 
					}
					speedPerPiecePerLane [i] [lane] = v0;
				}
			}
			if (!changed) {
				break;
			}
		}
		for (int i=0; i<pieces.Count; i++) {
			Console.WriteLine("Piece {0} speed {1:0.000}",i,speedPerPiecePerLane[i][0]);
		}

	}

	public static double bestSpeed (int pieceIndex, double inPieceDistance,int lane, double a, double da, double d2a)
	{		
		// Binary search on the speed (from 0 to 20)
		double high = 20, low = 0;
		while (high-low>0.002) {
			double mid = (high+low)/2;
			if(AngleModel.isStable(pieceIndex, inPieceDistance, mid, a, da, d2a, lane)){
				low=mid;
			}
			else{
				high=mid;
			}
		}
		return low-0.01;
	}

}

